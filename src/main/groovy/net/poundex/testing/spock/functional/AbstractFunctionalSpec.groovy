package net.poundex.testing.spock.functional

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.AnnotationConfigRegistry
import org.springframework.test.context.ContextConfiguration
import org.springframework.web.reactive.function.client.WebClient
import spock.lang.Specification

@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
		classes = ContextInitializer,
		initializers = ContextInitializer
)
abstract class AbstractFunctionalSpec extends Specification implements ApplicationResourceProvider {
	
	private final static ThreadLocal<Class<? super AbstractFunctionalSpec>> thisSpec = 
			new ThreadLocal<>()
	
	private static Optional<Class<?>> foundApplicationClass = Optional.empty();
	
	{
		thisSpec.set(this.class) 
	}

	@LocalServerPort
	int randomServerPort
	
	@Autowired
	ObjectMapper objectMapper
	
	@Autowired
	WebClient.Builder webClientBuilder
	
	private String webSocketEndpoint
	private String httpEndpoint

	void setup() {
		webSocketEndpoint = "ws://localhost:${randomServerPort}"
		httpEndpoint = "http://localhost:${randomServerPort}"
	}
	
	String getWebSocketEndpoint() {
		return webSocketEndpoint
	}
	
	String getHttpEndpoint() {
		return httpEndpoint
	}

	static class ContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		void initialize(ConfigurableApplicationContext applicationContext) {
			foundApplicationClass.or(() ->
					new ApplicationClassFinder().findApplicationClass(thisSpec.get()))
					.ifPresent {
						((AnnotationConfigRegistry)applicationContext).scan(it.getPackageName())
						foundApplicationClass = Optional.of(it)
					}
		}
	}
}
