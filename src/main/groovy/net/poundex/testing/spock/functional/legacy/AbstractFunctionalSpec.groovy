package net.poundex.testing.spock.functional.legacy

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import spock.lang.Specification

@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class AbstractFunctionalSpec extends Specification implements HttpClientFacade {

	@LocalServerPort
	int randomServerPort

	void setup() {
		httpEndpoint = "http://localhost:${randomServerPort}"
	}
}
