package net.poundex.testing.spock.functional

import com.netflix.graphql.dgs.client.codegen.BaseProjectionNode
import com.netflix.graphql.dgs.client.codegen.GraphQLQuery
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest
import graphql.language.SelectionSet
import graphql.schema.Coercing
import org.intellij.lang.annotations.Language

trait GraphQl {
	private final GraphQlTraitHelper traitHelper = new GraphQlTraitHelper(this.&getWebSocketEndpoint, (GraphQlApplicationResourceProvider) this)
	
	abstract String getWebSocketEndpoint()

	Map<Class<?>, ? extends Coercing<?, ?>> getScalars() {
		return [:]
	}

	public <T> List<T> queryList(Map<String, Object> args = [:], Class<T> klass, @Language("graphql") String query) {
		return traitHelper.queryList(klass, query, args)
	}
	
	public <T> T query(Map<String, Object> map = [:], Class<T> klass, @Language("graphql") String query) {
		return traitHelper.query(klass, query, map)
	}
	
	public Map<String, Object> query(Map<String, Object> map = [:], @Language("graphql") String query) {
		return traitHelper.query(query, map)
	}

	public <T> T query(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection) {
		return traitHelper.query(klass, new GraphQLQueryRequest(query, projection, scalars).serialize(), [:])
	}

	public <T> T query(Class<T> klass, GraphQLQuery query) {
		return traitHelper.queryForValue(klass, new GraphQLQueryRequest(query, (SelectionSet) null, scalars).serialize(), [:])
	}


	public <T> List<T> queryList(Class<T> klass, GraphQLQuery query, BaseProjectionNode projection) {
		return traitHelper.queryList(klass, new GraphQLQueryRequest(query, projection, scalars).serialize(), [:])
	}

}