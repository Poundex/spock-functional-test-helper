package net.poundex.testing.spock.functional

import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus

trait Json {
	
	private final JsonTraitHelper traitHelper = new JsonTraitHelper(this.&getHttpEndpoint)
	
	abstract String getHttpEndpoint()

	public <T> T get(String uri, Class<T> klass) {
		return traitHelper.get(uri, klass)
	}
	
	public <T> T post(String uri, Object payload, Class<T> klass) {
		return traitHelper.post(uri, payload, klass)
	}
	
	public <T> T postFile(String uri, String partName, Resource resource, Class<T> klass) {
		return traitHelper.postFile(uri, partName, resource, klass)
	}

	byte[] getRaw(String uri) {
		return traitHelper.getRaw(uri)
	}

	HttpStatus getStatus() {
		return traitHelper.lastResponse.statusCode
	}
}