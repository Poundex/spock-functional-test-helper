package net.poundex.testing.spock.functional;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.graphql.dgs.client.GraphQLError;
import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.MonoGraphQLClient;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.xmachinery.spring.graphql.client.ArgumentCoercingGraphQlClientDecorator;
import org.intellij.lang.annotations.Language;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class GraphQlTraitHelper {

	
	private final Supplier<String> httpEndpointSupplier;
	private final GraphQlApplicationResourceProvider spec;

	private ObjectMapper objectMapper;

	private MonoGraphQLClient $graphQlClient;

	private MonoGraphQLClient getGraphQlClient() {
		if($graphQlClient == null)
			$graphQlClient = initClient();

		return $graphQlClient;
	}

	private MonoGraphQLClient initClient() {
		this.objectMapper = spec.getObjectMapper();
		return ArgumentCoercingGraphQlClientDecorator.wrap(
				MonoGraphQLClient.createWithWebClient(spec.getWebClientBuilder()
						.baseUrl(httpEndpointSupplier.get() + "/graphql")
						.build()),
				spec.getGraphQlSource());
	}

	public <T> List<T> queryList(Class<T> klass, @Language("graphql") String query, Map<String, Object> map) {
		return extractList(klass, query(query, map));
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> query(@Language("graphql") String query, Map<String, Object> map) {
		Mono<GraphQLResponse> response = getGraphQlClient().reactiveExecuteQuery(query, map);
		List<GraphQLError> responseErrors = new ArrayList<>();
		Map<String, Object> responseData = response
				.doOnNext(r -> responseErrors.addAll(r.getErrors()))
				.map(GraphQLResponse::getData)
				.block();
		
		if( ! responseErrors.isEmpty())
			throw new GraphQlResponseHasErrors(responseData, responseErrors);
		
		return responseData;
	}
	
	public <T> T query(Class<T> klass, @Language("graphql") String query, Map<String, Object> map) {
		return extractSingle(klass, query(query, map));
	}

	public <T> T queryForValue(Class<T> klass, @Language("graphql") String query, Map<String, Object> map) {
		return extractValue(klass, query(query, map));
	}

	private <T> T extractSingle(Class<T> klass, Map<String, Object> realResponse) {
		return extract(realResponse, Map.class, objectMapper.getTypeFactory().constructType(klass));
	}

	private <T> List<T> extractList(Class<T> klass, Map<String, Object> realResponse) {
		return extract(realResponse, List.class, objectMapper.getTypeFactory().constructCollectionType(List.class, klass));
	}

	private <T> T extractValue(Class<T> klass, Map<String, Object> realResponse) {
		return extract(realResponse, klass, objectMapper.getTypeFactory().constructType(klass));
	}

	private <R> R extract(Map<String, Object> realResponse, Class<?> containerType, JavaType valueType) {
		Assert.notEmpty(realResponse, "Response has no components");
		
		Supplier<String> responseShapeError = () -> 
				"Expected response to have single " + containerType.getSimpleName() + " component: \n" +
						"  Response has " + realResponse.size() + " components: \n" + 
						realResponse.entrySet().stream()
								.map(entry -> "    * " + entry.getKey() + " => " + 
										(entry.getValue() instanceof Map ? "Map" : "List"))
						.collect(Collectors.joining("\n"));

		Object responseValue = realResponse.values().iterator().next();
		if(responseValue == null)
			return null;

		Assert.isTrue(realResponse.keySet().size() == 1 && 
				containerType.isAssignableFrom(responseValue.getClass()), responseShapeError);

		return Try.of(() -> objectMapper.writeValueAsString(responseValue))
				.mapTry(json -> objectMapper.<R>readValue(json, valueType))
				.get();
	}
}
