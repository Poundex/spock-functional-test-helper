package net.poundex.testing.spock.functional;

import org.springframework.graphql.execution.GraphQlSource;

public interface GraphQlApplicationResourceProvider extends ApplicationResourceProvider {
	GraphQlSource getGraphQlSource();
}
