package net.poundex.testing.spock.functional;

import com.netflix.graphql.dgs.client.GraphQLError;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
public class GraphQlResponseHasErrors extends RuntimeException {
	private final Object result;
	private final List<GraphQLError> responseErrors;

	@Override
	public String getMessage() {
		return String.format("""
 The GraphQl response contains errors.
   Partial response:
     %s
   Errors: %s
 """, result, "\n" + responseErrors.stream()
				.map(Object::toString)
				.map(s -> "    * " + s)
				.collect(Collectors.joining("\n")));
	}
}
