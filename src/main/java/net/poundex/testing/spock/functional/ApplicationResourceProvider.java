package net.poundex.testing.spock.functional;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.graphql.execution.GraphQlSource;
import org.springframework.web.reactive.function.client.WebClient;

public interface ApplicationResourceProvider {
	ObjectMapper getObjectMapper();
	WebClient.Builder getWebClientBuilder();
}
