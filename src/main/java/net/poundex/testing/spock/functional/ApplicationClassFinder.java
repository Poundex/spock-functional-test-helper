package net.poundex.testing.spock.functional;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.AnnotatedClassFinder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;

public class ApplicationClassFinder {
	
	private static final int MAX_PACKAGE_SCOPE = 3;
	
	public Optional<Class<?>> findApplicationClass(Class<?> declaringClass) {
		LinkedList<String> packageParts = Arrays.stream(
						declaringClass.getPackageName()
								.split("\\."))
				.collect(Collectors.toCollection(LinkedList::new));

		do {
			Class<?> found = new AnnotatedClassFinder(SpringBootApplication.class).findFromPackage(
					String.join(".", packageParts) + ".**");
			if(found != null)
				return Optional.of(found);

			packageParts.removeLast();
		} while(packageParts.size() >= MAX_PACKAGE_SCOPE);

		return Optional.empty();
	}
}
