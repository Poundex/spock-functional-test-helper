package net.poundex.testing.spock.functional;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class JsonTraitHelper {
	
	private final Supplier<String> httpEndpointSupplier;
	private final RestTemplate restTemplate = new RestTemplate();
	
	private ResponseEntity<?> lastResponse;

	public <T> T get(String uri, Class<T> klass) {
		return restTemplate.getForObject(url(uri), klass);
	}
	
	public <T> T post(String uri, Object payload, Class<T> klass) {
		return respond(restTemplate.postForEntity(url(uri), payload, klass));
	}

	public <T> T postFile(String uri, String partName, Resource resource, Class<T> klass) {
		return respond(restTemplate.exchange(RequestEntity.post(url(uri))
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.body(new LinkedMultiValueMap<>(Map.of(partName, List.of(resource)))), klass));
	}

	public byte[] getRaw(String uri) {
		return respond(restTemplate.exchange(RequestEntity.get(url(uri)).build(), byte[].class));
	}
	
	@SuppressWarnings("unchecked")
	<T> ResponseEntity<T> getLastResponse() {
		return (ResponseEntity<T>) lastResponse;
	}

	private <T> T respond(ResponseEntity<T> response) {
		lastResponse = response;
		return response.getBody();
	}

	private URI url(String path) {
		return URI.create(httpEndpointSupplier.get()).resolve(path);
	}
}
